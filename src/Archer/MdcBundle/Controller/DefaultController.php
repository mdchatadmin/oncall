<?php

namespace Archer\MdcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ArcherMdcBundle:Default:index.html.twig');
    }

	public function loadDepartmentAction(Request $request)
	{
		$department_name = $request->request->get('TargetPerson', null);
		$tpl = array(
			'Billing' => 'billing.html.twig',
			'Front Desk' => 'front-desk.html.twig',
			'Doctor' => 'doctor.html.twig'
		);
		if (key_exists($department_name, $tpl)) {
			return $this->render('ArcherMdcBundle:Default:'.$tpl[$department_name], array('TargetPerson' => $request->request->get('TargetPerson', null)));
		}
	}

	public function demowidgetAction()
	{
		return $this->render('ArcherMdcBundle:Default:mediacallgroup.html.twig');
//		return $this->render('ArcherMdcBundle:Default:demowidget.html.twig');
	}

	public function sendWidgetMessageAction()
	{
		$response = array('msg' => 'Your message is sent');
		die(json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP));

//		$this->get('request')->request->all()
	}

	public function getGroupMembersAction($group_name)
	{
		$members = array(
			'Billing' => array(
				1 => 'Bill Gates',
				2 => 'Tom Hanks',
				3 => 'Brus Lee'
			),
			'FrontDesk' => array(
				4 => 'Arnold Swarceneger',
				5 => 'Silwester Stallone',
				6 => 'Alen Delon'
			),
			'Doctor' => array(
				7 => 'Jekky Chan',
				8 => 'Penelopa Cruz',
				9 => 'Sacha Grey'
			)
		);

		$response = array();

		if (key_exists($group_name, $members)) {
			$response = $members[$group_name];
		}
		die(json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP));
	}
}
