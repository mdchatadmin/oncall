jQuery.noConflict();
(function($) {
	$(function() {
		$('#send-btn').on('click', function() {
			if ($("#dob_d").length && $("#dob_m").length && $("#dob_y").length) {
				if ($("#dob_d").val() != '' && $("#dob_m").val() != '' && $("#dob_y").val() != '') {
					var dob = $("#dob_m").val() + '-' + $("#dob_d").val() + '-' + $("#dob_y").val();
					$('#dob').val(dob);
				} else {
					alert('Date Of Birth is incorrect');
					return false;
				}
			}
			var send_msg_url = "https://altprod.mdchat.com/oncall_api";//window.location.origin + '/send-widget-message/';
			var form_values = {};
			$(this).parents('form').find('[name]').each(function(){
				form_values[$(this).attr('name')] = $(this).val();
			});

			//$('.after-send').show();
			//$('.before-send').hide();
			$.ajax({
				type: "POST",
				url: send_msg_url,
				crossDomain: true,
				data: form_values,
				dataType: "json"
			}).always(function() {
                alert("Thank you. Your message has been sent");
                $('.cancel').click();
            });
			return false;
		});

		$('#submit-to-department').on('click', function(){
			if ($("#target-person-select").length) {
				if ($("#target-person-select").val() === '') {
					alert('Select department please');
					return false;
				}
			}
			$(this).parents('form').submit();
			return false;
		});

		$('.cancel').on('click', function(){
			$(this).parents('form').attr('action', '/mdc/');
			$(this).parents('form').submit();
			return false;
		});
	});
})(jQuery);